package com.yaremchuk.view;

@FunctionalInterface
public interface Printable {
    void print();
}
