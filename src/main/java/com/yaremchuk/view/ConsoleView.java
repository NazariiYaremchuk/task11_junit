package com.yaremchuk.view;

import com.yaremchuk.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ConsoleView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;
    private static Logger logger = LogManager.getLogger(ConsoleView.class);

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Get max sequence.");
        menu.put("2", "2 - Get area String");
        menu.put("Q", "Q");
    }

    public ConsoleView() {

        controller = new Controller();
        input = new Scanner(System.in);


        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::getSequence);
        methodsMenu.put("2", this::getAreaString);
    }

    private void getSequence() {
        logger.info(controller.getMaxSequence());
    }
    private void getAreaString() {
        logger.info(controller.getMinesweeperSArea());
    }



    //-------------------------------------------------------------------------
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
}

