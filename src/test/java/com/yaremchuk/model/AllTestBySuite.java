package com.yaremchuk.model;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({MinesweeperTest.class, PlateauTest.class})
public class AllTestBySuite {

}
